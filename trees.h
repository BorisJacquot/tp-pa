/*
 * JACQUOT Boris
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node {
	int val;
	struct node *left;
	struct node *right;
}Node, *PtNode, *Tree;
/* These typedefs are optionnel, you may use them */


/* Constructs a new tree from a value for the root node, a given left tree and a given right tree */
void cons_tree(struct node **, int, struct node *, struct node *);

/* Make an empty tree */
void mk_empty_tree(struct node **);

/* Is the tree empty ? */
bool is_empty(struct node *);

/* Is the tree a leaf ? */
bool is_leaf(struct node *);

/* Adds the value (int) to the binary search tree,
 * it must be ordered.
 * Duplicate values are valid.
 */
void add(struct node **, int);

/* Prints the values of the tree in ascendant order */
void print_tree(struct node *);

/* Builds a tree adding values of the file */
void load_tree(FILE *, struct node **);

/* Frees all the tree's memory */
void free_tree(struct node **);

bool tree_search(struct node *, int);

bool del_node(struct node **, int);

int count_nodes(struct node *);

void array_to_tree(int *, struct node **);

void tree_to_array(int *, struct node *);
