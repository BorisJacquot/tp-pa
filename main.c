/*
 * JACQUOT Boris
 */

#include <stdio.h>
#include "trees.h"


int main (int argc, char *argv[]) {
	if (argc < 2) {
		perror("Il manque un arg");
		exit(EXIT_FAILURE);			// on vérifie qu'il y a un arg
	}

	struct node *arbre;
	FILE *fp;

	if ((fp = fopen(argv[1],"r")) == NULL){ 		// test ouverture fichier
		perror("Impossible d'ouvrir le fichier");
		exit(EXIT_FAILURE);
	}
	
	mk_empty_tree(&arbre);	// création arbre vide
	load_tree(fp, &arbre);	// lecture du fichier et remplissable de l'abre
	print_tree(arbre);		// affichage de l'arbe
	printf("\n");			// sinon c'est moche
	printf("1 est dans l'arbre ? : %d\n", tree_search(arbre, 1));
	printf("Nombre de noeuds : %d\n", count_nodes(arbre));
	free_tree(&arbre);

	int t[10] = {1,2,3,4,5,6,7,8,9,10};
	struct node *arbre2;
	mk_empty_tree(&arbre2);
	array_to_tree(t, &arbre2);	// test de la fonction
	print_tree(arbre2);
	printf("\n");

	int tab[20];
	tab[0] = -1;	// init du tableau pour qu'il fonctionen avec la fonction
	tree_to_array(tab, arbre2);
	for (int i = 0; i < 10; ++i)
	{
		printf("%d\n", tab[i]);	// affichage du tableau
	}

	free_tree(&arbre2);

	return EXIT_SUCCESS;
}

