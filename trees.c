/*
 * JACQUOT Boris
 */

#include "trees.h"

// construction of a tree by pointer
void cons_tree(struct node ** ptr_tree, int val, struct node *left, struct node *right)
{
	if (*ptr_tree == NULL){ 							// on vérifie que l'arbre n'existe pas déjà
		*ptr_tree = malloc(sizeof(struct node));		// on alloue le pointeur d'arbre
		(*ptr_tree)->val = val;							// on init l'arbre
		(*ptr_tree)->left = left;
		(*ptr_tree)->right = right;
	}
}


// initialize un empty tree
void mk_empty_tree(struct node ** ptr_tree)
{
	*ptr_tree = NULL;
}

// is tree empty?
bool is_empty(struct node *tree)
{
	return tree==NULL;
}

// is tree a leaf?
bool is_leaf(struct node *tree)
{
	return(!is_empty(tree) && is_empty(tree->left) && is_empty(tree->right));
}

// add x in a bst wtr its value.
void add(struct node **ptr_tree, int x)
{
	if (is_empty(*ptr_tree)) {
		cons_tree(ptr_tree, x, NULL, NULL);
	} else {
		if ((*ptr_tree)->val > x) {
			add(&((*ptr_tree)->left), x);		// si val > x on va à gauche
		} else if ((*ptr_tree)->val == x) {
			return; 							// si la valeur est = à x on arrête
		} else {
			add(&((*ptr_tree)->right), x);		// si val < x on va à droite
		}
	}
}

// print values of tree in ascendant order
void print_tree(struct node *tree)
{
	if (!is_empty(tree)) {
		print_tree(tree->right);
		printf("%d ", tree->val);
		print_tree(tree->left);
    }
}

// build a tree "add"ing values of the file fp
void load_tree(FILE *fp, struct node **ptr_tree)
{
	int n;
	while(fscanf(fp, "%d", &n) != EOF) {
		add(ptr_tree, n);
	}
}

// Free all memory used by the tree
void free_tree(struct node **ptr_tree)
{
	if (!is_empty(*ptr_tree)) {
		free_tree(&((*ptr_tree)->left));
		free_tree(&((*ptr_tree)->right));
		free(*ptr_tree);
	}
}

// recherche ul'existance d'une valeur dans un arbre
bool tree_search(struct node * tree, int x) {
	if (tree == NULL) 
    	return false;
	else if (x == tree->val) 
    	return true;
	else if (x < tree->val)
    	return tree_search(tree->left, x);
    else
    	return tree_search(tree->right, x);
}


// supprime un noeud
bool del_node(struct node **ptr_tree, int x) { // après avoir passé littéralement 2h sur ce code, qui ne fonctionne pas, j'abandonne. (je n'ai pas trouvé  del_subtree )
    if (!is_empty(*ptr_tree)) {
        struct node * tmp;
        if ((*ptr_tree)->val == x) {
            if ((*ptr_tree)->left != NULL) {
                tmp = (*ptr_tree)->left;
                while (tmp->right != NULL) {
                    tmp = tmp->right;
                }
                tmp->right = (*ptr_tree)->right;
                free_tree(&(*ptr_tree)->right);
                (*ptr_tree) = (*ptr_tree)->left;
            }
            else {
                tmp = (*ptr_tree)->right;
                free((*ptr_tree)->right);
                (*ptr_tree) = tmp;
            }
            return true;
        } else {
            if ((*ptr_tree)->val > x) return del_node(&((*ptr_tree)->left), x);
            if ((*ptr_tree)->val < x) return del_node(&((*ptr_tree)->right), x);
        }
    }
    return false;
}

// compte le nombre de noeuds
int count_nodes(struct node * tree) {
	int somme = 0;
	if (tree != NULL) {
		somme = count_nodes(tree->left);
		somme++;
		somme += count_nodes(tree->right);
	}
	return somme;
}

// transforme un tableau en arbre
void array_to_tree(int tab[10], struct node ** ptr_tree) {
	for (int i = 0; i < 10; ++i) {
		add(ptr_tree, tab[i]);		
	}
}

// transforme un arbre en talbeau
void tree_to_array(int * tab, struct node * tree) {
	if (!is_empty(tree)) {
		tree_to_array(tab,tree->left);
		int i = 0;					// nécessite un tableau initialisé à tab[0] = -1
		while (tab[i] != -1) i++;	// on cherche le -1 qui indique la fin du tableau
		tab[i] = tree->val;			// quand on a l'indice du -1 pn le remplace par la valeur de l'arbre
		tab[i+1] = -1;				// on met la valeur suivante à -1 pour repérer la fin du tableau
		tree_to_array(tab,tree->right);
    }
}