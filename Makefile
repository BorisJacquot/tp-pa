DEBUG=yes
CC=gcc

ifeq ($(DEBUG),yes)
	CFLAGS=-W -Wall -Wextra -g
else
	CFLAGS=-W -Wall -Wextra
endif

EXEC=main
SRC= $(wildcard ./*.c)
OBJ= $(SRC:.c=.o)

all: $(EXEC)

main: $(OBJ)
	$(CC) -o $@ $^

main.o: ./trees.h
	$(CC) -o $@ -c main.c $(CFLAGS)

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean hardclean

clean:
	rm -rf *.o

hardclean: clean
	rm -rf $(EXEC)